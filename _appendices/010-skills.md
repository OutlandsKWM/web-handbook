---
title: Skills
---
Isn't every appendix a placeholder?

You can make unordered lists:
* item 1
* item 2

Ordered lists:
1 item 1
2 item 2
2 item 3 

[text for the link](http://www.google.com)

    4 spaces
    will show code <html> for instance

**double will emphasis**

*single will italicise* 

# h1

## h2

### h3

#### h4

## Techsoup

Techsoup is a verification service and marketplace for Nonprofit organizations. Techsoup has also partnered with 70 other organizations to provide similar services around the globe. Techsoup and its partner organizations will hereafter be referred to collectively as Techsoup. Once your nonprofit status is verified you will have access to a variety of products and services that are free or significantly discounted. Other organizations, Google for example, use Techsoup to verify your nonprofit status for access to additional services that they make available for nonprofits. To access these products and services you must first complete the registration and verification process. 

Signing up for Techsoup requires setting up a new account. Information that you will need to set up your account includes the following. This list is subject to change. 

* __Community Nickname__: This is the name that your organization will be assigned within Techsoup. This name should be unique and descriptive, for example: Society for Creative Anachronism, Kingdom of Atlantia. A good general practice is to use the same name as the legal name under which your group is incorporated. 
* __Email Address__: It is recommended that this account be attached to the Kingdom Webminister's email so that future KWeb Officers have access to the account for managment.
* __Incorporation ID__: *Important: This is where most groups experience the most trouble.* You must use the nonprofit legal identifier (Tax EIN in the United States) number for YOUR GROUP, NOT FOR SCA CORPORATE. Contact your Kingdom Seneschal and/or Exchequer for this information.  

Once you complete the sign up form there will be a review period for Techsoup to evaluate your request. During this time you can log in to the website and browse the available discounts and services. Once your nonprofit status is verified you will receive an email notification that you are authorized to request products and services. 

## Wordpress

Selected themes & plugins should be updated for new versions of WordPress on a regular basis.
- Themes & plugins that have not been tested with the current and last version of WP should be replaced.


## Minimal Mistakes

Static websites through git and markdown


# Cloud systems

## Google for Non Profits

How to organise your Google setup. 

## Google Forms & Automation

Automating processes using forms, Google sheets and javascript

## OneDrive

## Google Drive

## Github

## Social skills/Management skills 
