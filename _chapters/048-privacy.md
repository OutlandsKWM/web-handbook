---
title: Data Protection/Information Security/Privacy Policy
---

## What is Protected Information?

Briefly, protected information is any information that requires special handling.  Within the SCA we encounter three primary types of protected information: personal data or personally identifiable information, confidential information, and very occasionally credit card information.  Each of these has different requirements as to how it must be stored, protected, and disposed of when we no longer need it, and these requirements come to us from local, state, provincial, national, and even international law, tradition within the Society, and in the case of credit cards, the Payment Card Institute or "PCI", which is an industry coalition among the major credit card companies.

## Protected Information Lifecycle

Every piece of information has a lifecycle: it is created, it is stored, it is used, and it is destroyed.  What exactly comprises each of those steps varies depending on the information itself: web pages may be stored nearly indefinitely and revised repeatedly over time, social media posts may be written, sent, read, and destroyed nearly instantly.  This lifecycle and what constitutes each of those steps is more important for protected information than it would be for public information such as posts or web pages.  In particular, it is crucial that we remember the final step of the lifecycle: protected information, when it is no longer needed, must be actively destroyed or we will have to spend time, effort, and possibly money on continuing to protect information that no longer has value to us.

## Lifecycles By Class

_Credit Card Information_: This has the most critical and also the simplest lifecycle of the forms of protected information we may encounter:   We are currently prohibited from taking credit cards online in any form other than the Paypal process by Society Financial Policy, we should not be at any time collecting, storing, or receiving credit card information.  If you do encounter this form of protected information, destroy it thoroughly and immediately.

_Confidential Information_: The Webministry is often called upon to provide online services that are not web-related, even though this is technically outside of our original mission.  A common such service is mailing lists intended for awarded orders to be polled and to discuss candidates for membership in the order.  Another is for members of the populace to make recommendations to Their Majesties nominating candidates for awards, whether those are polled or otherwise.  These communications are generally held, by tradition, to be of the utmost privacy and requiring of special handling to ensure, to the best of our ability, that such communications not leak to the general populace.  Such information then should be stored in dedicated locations that are not used to store public information and that are protected by appropriate authentication to ensure only the intended users of such information can do so.  It should be destroyed promptly when it is no longer needed, such as immediately at the end of a polling period, at the end of the reign in which it would apply, or at some other time according to your Kingdom's Laws and traditions.  Mailing lists for such orders should generally be configured so as to not archive messages, as archives then would require frequent deletion in order to maintain the confidentiality of the contents as new members are added.

_Personal Data_: This is the most common and the thorniest of the forms of protected information we will typically encounter.  To quote the European Union's "General Data Protection Regulation" or "GDPR", Personal Data "means any information relating to an identified or identifiable natural person (‘data subject’); an identifiable natural person is one who can be identified, directly or indirectly, in particular by reference to an identifier such as a name, an identification number, location data, an online identifier or to one or more factors specific to the physical, physiological, genetic, mental, economic, cultural or social identity of that natural person"<sup>1</sup>.  A common exception to this is any apparently personal data that can be found in sources of public information such as property records or voter rolls, although this exception depends on exactly where you are and what legal jurisdictions you fall under.

The most common question about what constitutes Personal Data is "Is a Society Name considered Personal Data?" and the answer depends on what other information surrounds it.  A Society Name by itself does not identify a "natural person" without additional context that the average person will not have.  Neither would a Society membership number, as while these might be considered "identification numbers", they are not publicly indexed in a way that would tie back to a "natural person".  However, either of these in combination with a personal email address or mailing address would enable identiciation of the owner of the name and the combination would then be protected.  It is likely safer to consider Society Names in combination with other pieces of personal information to be protected as the default unless it is clearly not identifying: a name and list of awards, such as would be posted on an Order of Precedence page does not identify the person and would be fine, but does some other combination qualify?  As an example of an unexpected combination that is uniquely identifying: within the United States 87% of people can be uniquely identified by the combination of their gender, ZIP code, and full date of birth.

There are also situations around personal data which may morally require careful handling even if there are no laws or regulations that would apply: for example, where someone has an order of protection against a stalker and has carefully hidden their contact information from that individual, they are trusting us to respect their wishes concerning their information.  As we often will not be aware of such situations, we must presume a desire for privacy if we do not have explicit consent otherwise.

At the time of creation of Personal Data the legitimate purpose for which we are collecting the information should be designated, along with the duration of that legitimate purpose.  For example, information we are collecting for financial reasons will need to be kept for 7 years<sup>2</sup> to comply with legal requirements, after which time it will be destroyed.  Such information then must be stored securely for the duration of its term.  Actual information security guidelines are beyond the scope of this document, a few are listed below under Resources.  Taking advantage of cloud services such as Google Drive, which is itself certified for security, requiring long passwords, and ensuring that only those people who require access to the information are permitted access to the information, will often be enough for our purposes.  Finally, once the information is no longer needed, either actively or by regulatory requirement, it must be destroyed in such a way that it can not be recovered.

## Handling Protected Information

### Data Storage and Access
All protected information held in electronic format in group-controlled accounts should be hosted by secure third-party data providers (eg. Google Docs) and is only to be accessed, used or shared appropriately by people with legitimate interest, such as shire officers and event team members.

### General Data

The group holds some information for the proper, legal and safe governance and organisation of the legitimate interests and obligations of the group. This includes, but is not limited to: accounts, correspondence, accident reports, safeguarding information and information about venue bookings.  This is not technically protected data, but the availability and integrity of this data should still be protected.

### Personal Data

For all events for which online payments are used, information on those individuals who partake of online payments shall be considered to be financial information and shall be retained in electronic form according to financial policy, after which it must be securely deleted.

In order to attend or register for an event with advance registration, participants are asked to provide some details, which will vary by event but are likely to include:

- Legal name and SCA name – so we know who is coming
- Contact details, usually phone numbers and email addresses – so we can provide further information about the event, including notification of any changes
- Membership status within the SCA or an affiliate body of the SCA - in order for us to comply with Society reporting of attendance
- Any food allergies or other special dietary needs – for events where food is prepared
- Other information provided to the event team specific to the event eg. physical limitations on bunk bed use

We should ask for explicit consent to hold and use event registration data for the purposes of organising and running the event.
Full information on event attendees is available to the core event team only (usually event steward, deputy event steward, head cook and registration steward). Some information, excluding contact information, may also be made available to other event team members (such as kitchen helpers, volunteers at the registration desk, heralds) where necessary.

Information regarding online payments are collected and processed by the bank. Only people with a legitimate interest, normally the Exchequer and/or a registration steward, have access to information provided on the bank statement about payments.

### Intellectual Property Consent Forms

Consent forms for the use of images, recordings, other forms of media, or application code that has been developed, or pertaining to the public release of personal data for official reasons, will by necessity contain legal names and contact information, as well as Society name, and will be stored for so long as they remain in effect.  That is, so long as the intellectual property described may still be in use, or the personal data must continue to be released.

### Membership

By requesting to join SCA, Inc, consent is given for membership details to be retained in a 3rd party data application with appropriate login security and policies.  Member details will only be accessed by appropriate individuals with legitimate purposes (ie the Membership Secretary, the Seneschalate to be able to run polls, etc.)

### Mailing Lists

Any SCA-related mailing lists operated by any Kingdom or branch will by nature require email addresses, user names, and passwords, and will be held securely by the mailing list administrator.  Self-service unsubscription mechanisms should be available, and requests for assistance unsubscribing must be fielded in a timely manner.

### Sharing Information with Third Parties

Information may be shared with appropriate people within the SCA structures where proper and necessary to the legitimate interests of the group.
We must not pass personal data to third parties outside the SCA, other than for cloud storage or if legally required to do so.
If event management or ticketing systems are used in order to collect event information we will only use reputable providers with appropriate data protection policies.

### Updating or deleting your details

Requests to update details within, or delete in its entirety, an individuals protected data, must be addressed in as timely a manner as practicable.

## Release Form for Protected Information

**WE NEED TO DECIDE IF FORMS ARE GOING IN THE HANDBOOK AND THEN ADD THIS RELEASE FORM HERE IF APPROPRIATE**



Notes:<br>
1: https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32016R0679&from=EN#d1e4227-1-1, Article 4, paragraph 1<br>
2: Often 7 years, but may vary according to specific circumstances and jurisdiction.<br>

Resources:<br>
1. https://cisecurity.org<br>
2. https://www.nist.gov/cyberframework<br>
