---
title: Website Construction
---

Kingdom websites should include at minimum:
* A description of the kingdom, including modern boundaries, and links to existinglocal groups’ websites.
* A list of kingdom officers and royals.
* A current calendar of upcoming events.
* Links to current kingdom law.
* Information for newcomers.
* A disclaimer listing who is responsible for the site, including an email address –usuallywebminister@kingdom.org.

Care should be taken to make the kingdom website accessible on multiple platforms,including mobile devices.

Society  officer  documents  should  be  linked  from  [sca.org](https://www.sca.org/),  rather  than  posting  down-loaded copies, so that the most recent/updated version is alway linked.  (Link the pagerather than the specific document, in case of changes.)

Personal information should not be displayed on kingdom websites unless the kingdomweb minister has the appropriate release form on file ( Electronic copy acceptable and encouraged; no need to go paper old school. ).  Photographs should either begeneral/group shots, or have a model release on file.

