---
title: Release Forms
---

The SCA respects the rights that people have in their creative works and in images that show their likeness.

Webministers are responsible for understanding the types of information subject to these protections, and ensuring that written permission is properly obtained and archived for all applicable material which is published or used within their areas of responsibility.


## Purpose

The practice of obtaining written permission for material created by or about other people reflects our core values, and also helps to avoid disputes by ensure that the Society does not infringe on other people's legally-protected rights. 

The details of the relevant laws are complicated and vary between jurisdictions, but at a very high level we can think of these protections as falling into three broad categories:

* Privacy: information that identifies a person or allows them to be contacted should not be disclosed without their consent.

* Likeness: a person's image should not be used to promote things without their consent.

* Copyright: a person's original writing and artistic creations should not be copied or published without their consent. 


## Release Forms

The SCA uses written documents called release forms to record that it is permitted to use material created by or depicting other people.

There are official forms available as PDF documents from the SCA's web site that must be used to record permission in the cases set out below.

There are three official forms:

* Model Release Form
https://www.sca.org/wp-content/uploads/2019/12/ReleaseModelFillable.pdf

* Photographer Release Form
https://www.sca.org/wp-content/uploads/2019/12/ReleasePhotographerFillable.pdf

* Creative Work Copyright Assignment / Grant of Use Form
https://www.sca.org/wp-content/uploads/2019/12/ReleaseCreativeFillable.pdf

The PDF documents contain fillable blank fields that allow values to be typed in, or they may be printed out and filled in by hand. 

If the form is completed electronically, it may be digitally signed using a tool such as Adobe Acrobat; otherwise it must be signed by hand.

Printed forms may be delivered in person or by mail, or may be scanned and delivered electronically.

The official forms were created by an attorney and must not be modified in any way other than by filling in the appropriate information for each field. 


### Receiving Forms

Permission should be collected prior to publication. Avoid publishing material and then attempting to retroactively obtain permission for it.

You may make blank release forms available at events, but signing model release forms must not be presented as a requirement to participate in an event or hold an office.


### Archiving Forms

Webministers may receive and store signed forms electronically or on paper or both, but must be able to supply a printed copy of the form upon request, so they must be maintained in a reliable file system. 

For example:

* Some webministers print out forms that they receive electronically, and combine them with forms that they receive on paper, and store them all in a series of file folders sorted by type and the name of the individual.

* Some webministers digitize forms that they receive on paper and combine them with forms that they receive electronically, and store them all in a password-protected cloud-based file service.

Additionally, webministers are responsible for turning over copies of those forms to their successor in office.


## Personal Information

Individuals' personal information must not be published without their permission.

Personal information includes:
* Modern name;
* Home or work address;
* Phone numbers;
* Personal e-mail address.

The following are not considered personal information:
* Society names;
* Society membership numbers;
* Society-managed or official e-mail addresses such as webminister@sca.org

For example: 

* Permission is not required to list an officer on a website in the form of "Seneschal: Lord Robert of the Raised Hand \<seneschal@branch.domain.org\>". 

* You must obtain permission before including someone's modern name or personal contact details in a public listing of officers or event attendees.

* Permission is not required to store personal information in a password-protected electronic data system accessible only to branch officers, because that is not considered to be publishing it.

There is no official Society-provided form for documenting permission to publish personal information. A written statement from the relevant individual that describes which information may be published is sufficient. 

If a person later revokes this permission, that information should be removed from websites and other public locations under the Society's control. 


## Model Releases

Photographs that are portrait-style or were taken in private settings must not be published without the permission of those pictured.

Either one of these criteria is sufficient to require a release:

* A photograph is considered to be portrait-style if it features one person or a small group of people, and is used in a way that emphasizes their identity.

* A photograph is considered to be taken in a private setting if it is taken at a non-public venue such as in someone's home, or in a private space within a venue such as a household's encampment at an event.

By contrast, releases are not required when both of these criteria are met:

* A photograph is not considered to be portrait-style if it features a large number of people or is used in a context which presents it as "news" such as depicting an activity occurring at an event.

* A photograph is considered to be in a public place when there is no expectation of privacy, including in the open areas of any SCA event such as court, martial contests, competitions, classes, or merchants' row.

For example:

* A model release is required to publish a photograph of Their Majesties on the home page of the kingdom website, even if it was taken at a public event, because this is considered a portrait-style image.

* A model release is required to publish a photograph of the Armored Combat Champion on a kingdom's "Roll of Champions" web page, even if their face is partially obscured by their helmet, because the use of the photograph emphasizes their identity and is thus considered a portrait-style image.

* A model release is required to publish a photograph from a kingdom choir rehearsal in someone's home, even if there is a large group and no one person is the focus of the image, because the photograph was taken in a private setting.

* A model release is not required for a photograph of Their Majesties presenting an award in court to be included in a web page about the event, because it was taken in a public place and the focus is on the activity taking place.

Model releases must be documented using the official form specified above.

If any person appearing in a photograph or other image requests its removal, it should be removed from websites and other public locations under the Society's control, even if there is a signed model release on file.


## Photographer Release

Photographs are considered creative works and must not be published without the permission of the photographer.

Permission to publish a photograph must be documented using the official releases form specified above.

Photographers generally understand that some level of cropping or color adjustment may occur during publication, but if you are significantly editing a photograph, you should review your modified image with the photographer and obtain their approval before publication.

* Photographs of sculptures and other three-dimensional artifacts are subject to this same type of protection and require permission to publish, just as a photograph of people or an event would. 

* Scans of documents or flat artwork are generally considered mechanical reproductions and do not require a photographer release from the person who made the scan. 

* In the United States, photographs that are functionally equivalent to a scan of documents or flat artwork do not require a photographer release. For example, in the United States you do not need a release from the photographer to publish a simple photograph of a painting that is taken square-on with even lighting, because this is not considered a creative work. (However, you might require a creative-work release from the painter.) If the photograph is taken "creatively," or if significant effort or expertise was invested, such as use of specalized equipment or extensive post-processing to improve the quality of the image, a photographer release should be collected. If you are outside of the United States, you should collect releases for all photographs unless you have clear guidance from your Kingdom's Seneschal that a specific category of photograph is not legally protected in your jurisdiction.

If a photographer later requests the removal of a photograph they took, it should be removed from websites and other public locations under the webministry's control, even if there is a signed photographer release on file.

> **[TODO:]** The model release and photographer release forms cover photographs, but groups are increasingly filming and distributing videos via the Internet; how can the webministry assist the Society in preparing a version of these release forms that covers video?


## Creative Work

Creative works must not be published without the permission of their creator.

Protected creative works include:

* original writing including articles, poems, stories and songs;
* art works including sculptures, paintings, and calligraphy.

The following are not considered protected creative works:

* officer announcements and reports;
* event notices and reports;
* meeting minutes;
* captions;
* material created prior to 1600, which is not covered by copyright law.

Webministers may publish material created for the express purpose of Society use by themselves and other officers including event stewards without the need for a creative-work form.

Creative work assignments and grants of use must be documented using the official form specified above.

> **[TODO:]** The release policies note that certain categories of content do not require a creative-works form, such as officer reports and event announcements, presumably on the grounds that they are not "creative works". Can we provide any guidelines or a systematic way of determining what qualifies? 
> 
> For example, if a branch's Historian sends around a brief monthly notice listing recent activities in the area, that presumably falls under the "correspondence from officers" that do not require a release -- but on the other hand if they gather up and a decade of such notices and produce a "History of the Barony of X" that probably does require a release. Is there some way to articulate the difference?

> **[TODO:]** Some of the work that is done within the webministry has traditionally not had releases executed for it, with the result that it is not always clear what rights the Society has in the custom web templates, event calendar software, or order-of-precedence databases it is using; how can the webministry establish a practice of ensuring that the developers who create these works either transfer their ownership to the Society or grant us an irrevocable license to use and modify them?
> 
> The "creative works" form seems like it might almost cover this need, but there are a couple of rough edges. 
> 
> * The CW form doesn't anticipate being used for software and doesn't have a checkbox for that.
> 
> * The CW form doesn't anticipate being used for collaborative projects and includes a statement that "I am the sole creator of this Work" which isn't applicable to software that multiple people are working on together.
> 
> * The CW form is intended to be signed after the work is created and is being given/licensed to the Society, but software is often a long-term project on which work continues after it is first deployed. The proper model for many software projects is to have contributors agree to license terms at the start of the project and then have all of their future contributions governed by the same agreement.



