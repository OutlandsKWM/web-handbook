---
title: Chain of Command
---

## Purpose

The purpose of a Chain of Command is to show who is responsible for what job and to identify who reports to whom in the organization. While we are an organization of volunteers, it is still vital to understand  lines of communication within the organization. 

## Responsibilities

Web ministers' primary responsibilities are the maintenance of the IT related infrastructure, which commonly includes websites and email solutions. When it comes content webministers primary role is fascilitating, either by uploading content provided or by enabling others to manage content directly. Web ministers execute oversight over content to ensure it complies with local standards and regulations. 

The content managed by the webminister for an SCA website typically consists of three types:

* Content originating from the various offices in the branch for which the relevant officer is responsible. Examples of this would be a local marshal handbook or guidelines from the seneschal on reporting. Web ministers and their team should exercise caution when managing this content as it is not their responsability.
* Content organisation: These would be pieces of content connecting different pieces of the website together. For instance a short text introducing potential participants to the activities in the society and directing them to the relevant officer pages or information on the current royals.  Web ministers and their team often pick up items in this category. Activity here is commendable but not required. 
* Overall look and feel: The structural pieces of the website(s), standards for the look and feel of the website with the intent to create a coherent experience for visitors. This could include graphics or translations of word documents to a html documents. This is the bread and butter work of web ministers and the core of their responsability.


Society webminister and Kingdom webministers provide guidance to local branches as requested or needed to ensure (Kingdom) policies are upheld. 

The Society webminister is responsible for establishing and maintaining society wide standards for Kingdom websites and IT.   Kingdom Web Ministers are responsible for setting kingdom policies which supplement but do not supercede society policies. Local web ministers are warranted by, and report to, the Kingdom Web Minister.

Kingdom webminister should maintain a roster of local branch web ministers who report to them.

Current Structure is as follows:
  * Board of Directors
    * Manager, Information Tech
      * Society Webminister
        * Kingdom Webministers
          * Principality Webministers (if applicable)
            * Local Group Webministers reporting to Principality
          * Local Group Webministers reporting to Kingdom

## Reporting deadlines

| Quarter | Kingdom | Society |
| ------ | ------ |------ |
| Domesday | | | 
| First | | |
| Second | | | 
| Third | | | 
| Fourth | | | 

The Domesday report is an annual report provided by each kingdom's Kingdom Webminister which should include a state of the webministry, as well as a listing of all warranted webministers, including local webministers.

The quarterly reports are intended to keep the lines of communication open, and should include information about how the webministry is doing, as well as issues that have arisen or solutions that have been implemented.  Quarterly reports are used to fuel the Society Webminister's quarterly reports, and it's hard to praise effort when the society webminister isn't informed.

## Warrants: how webministers are recognized at each level

Warrants are official recognition of the legal agency and standing of an officer within the corporate SCA structure. Kingdom webministers are officers and therefore require warrants. Warrant forms can be found in Appendix B of the Governing Documents of the Society for Creative Anachronism, Inc. (Corpora), and on the SCA’s web site. 

The Society Web Minister is warranted by the BoD (Board of Directors) in consultation with the VP (Vice-President) of IT and reports to the VP IT on a quarterly basis.  Kingdom Web Ministers are warranted by their kingdom Crown and Seneschal,  in consultation with the Society Web Minister, and report to the Society Web Minister on a quarterly basis.  

## Conflicts: how to resolve them

The organisational handbook for the SCA has an excellent section on grievances. Rather than copy paste this section here we shall refer to https://www.sca.org/wp-content/uploads/2019/12/govdocs.pdf Section X.B Grievances, currently on page 37
