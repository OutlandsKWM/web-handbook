---
title: Responsibilities
---
Kingdoms should have a data policy. This policy should as a minimum follow legal requirements. Examples can be found in the Appendix.
