---
title: Officer Requirements
---
Some stuff goes here

**Kingdom Webministers**

- Organization, coordination, delegation
- Fundamentals of project management
- Liaison with other Kingdom Officers and Crown
- Be able to communicate requirements and realities of technology

**Kingdom Webministry Staff**

- This will vary greatly from Kingdom to Kingdom - the technology platform for Kingdom A may not be identical to the platform for Kingdom B, so keeping a core staff with appropriate knowledge is vital
- Ongoing succession planning for all levels
