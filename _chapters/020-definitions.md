---
title: Definitions
---

2FA:

Alt text:

App:

Backup:

Breadcrumbs:

cPanel:

Code:

Copyright:

CMS: 

CreativeCommons:

DNS:

Domain: The web address in its simplest form (i.e. yahoo.com, sca.org, google.com)

Embed:

File Extension:

Firewall:

Freeware:

FTP:

GitHub:

Group Pages: Websites or pages for a specific group in Æthelmearc

GUI: 

Guild: A group of people within the SCA officially chartered by the Kingdom or a local Barony for a specific purpose, usually the promotion of a particular Art or Science

Hijack:

Host or Hosting: The company that is maintaining the servers on which web pages are stored

Household: A group of people within the SCA not recognized as an official SCA branch

Hypertext/HTML: 

Link:

Main Level:

Main Page: The index page or the first page that people see when visiting a website. This does not include splash pages

Markup (Language):

Nameserver:

Nested pages:

Officer Pages: websites or pages for an office or officer in a Kingdom

Official Site/Page: A website recognized as the website for that group or office, which is recognized by the group

Permissions:

Releases:

Responsive:

Server:

Social Media:

Splash Page: A greetings page, which may include animations or options to select what kind of detail you would like to see on the website.

Strong Password:

Trojan:

Virus:

Webpage: One page of web code

Website or Site: An entire grouping of pages hosted by a single entity OR A collection of web pages gathered together to represent an idea or theme

Webminister:

WHOIS:

WYSIWYG: 
